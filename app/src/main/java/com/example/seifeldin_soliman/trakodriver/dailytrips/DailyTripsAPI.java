package com.example.seifeldin_soliman.trakodriver.dailytrips;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by seifeldin_soliman on 12/2/2018.
 */

public interface DailyTripsAPI {


    @GET("trips")
    Call<GetDailyTripModel> getTrips(@Header("Authorization") String uesr_id);
}
