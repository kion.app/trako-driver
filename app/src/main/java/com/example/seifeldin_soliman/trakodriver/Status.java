package com.example.seifeldin_soliman.trakodriver;

import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 12/22/2018.
 */

public class Status {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
