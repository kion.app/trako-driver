package com.example.seifeldin_soliman.trakodriver.dailytrips;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;


import com.example.seifeldin_soliman.trakodriver.R;
import com.example.seifeldin_soliman.trakodriver.RetrofitInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DailyTripsActivity extends AppCompatActivity {

    SharedPreferences getUserData;
    private RecyclerView tripsRecyclerView;
    private DailyTripsAdapter tripsAdapter;
    private ArrayList<Trips> tripList;
    private ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_trips);
        tripsRecyclerView = (RecyclerView) findViewById(R.id.daily_trips_recycler_view);
        tripsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Intent intent = getIntent();
        getUserData = getApplication().getSharedPreferences("USER_PREF", Context.MODE_PRIVATE);
        String userId = getUserData.getString("userId", "0");
        progressBar = findViewById(R.id.progress_bar);
        getDailyTrips(userId);

    }

    public void getDailyTrips(String userId) {

        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = RetrofitInstance.retrofitConnection();
        DailyTripsAPI dailyTripsAPI = retrofit.create(DailyTripsAPI.class);

        Call<GetDailyTripModel> getDailyTripModelCall = dailyTripsAPI.getTrips(userId);

        getDailyTripModelCall.enqueue(new Callback<GetDailyTripModel>() {
            @Override
            public void onResponse(Call<GetDailyTripModel> call, Response<GetDailyTripModel> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful() && response.body() != null) {

                    String code = response.body().getStatus().getCode();

                    ArrayList<Trips> tripsList = response.body().getDailyTripsList();
                    if (code.equals("200")) {

                        tripsAdapter = new DailyTripsAdapter(DailyTripsActivity.this,tripsList);
                        tripsRecyclerView.setAdapter(tripsAdapter);

                    } else if (code.equals("8000")) {
                        Snackbar.make(findViewById(R.id.daily_trip), "failed to search for trips today , try again", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(findViewById(R.id.daily_trip), "Internal Error", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.daily_trip), "General Error", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetDailyTripModel> call, Throwable t) {
                Snackbar.make(findViewById(R.id.Login_activity), "Oops! Failed to download data, please check your internet connection", Snackbar.LENGTH_SHORT).show();

            }
        });
    }
}
