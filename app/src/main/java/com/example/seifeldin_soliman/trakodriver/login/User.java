package com.example.seifeldin_soliman.trakodriver.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 1/6/2019.
 */

class User {
    @SerializedName("id")
    private String userId;
    @SerializedName("name")
    private String userName;
    @SerializedName("mobile")
    private String userMobileNum;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMobileNum() {
        return userMobileNum;
    }

    public void setUserMobileNum(String userMobileNum) {
        this.userMobileNum = userMobileNum;
    }
}
