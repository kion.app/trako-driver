package com.example.seifeldin_soliman.trakodriver.dailytrips;

import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 1/5/2019.
 */

public class Trips {

    @SerializedName("id")
    private String DailyTripsId;
    @SerializedName("description")
    private String DailyTripsDescription;
    @SerializedName("status")
    private String DailyTripsStatus;
    @SerializedName("dateTime")
    private String DailyTripsDateTime;

    public String getDailyTripsId() {
        return DailyTripsId;
    }

    public void setDailyTripsId(String dailyTripsId) {
        DailyTripsId = dailyTripsId;
    }

    public String getDailyTripsDescription() {
        return DailyTripsDescription;
    }

    public void setDailyTripsDescription(String dailyTripsDescription) {
        DailyTripsDescription = dailyTripsDescription;
    }

    public String getDailyTripsStatus() {
        return DailyTripsStatus;
    }

    public void setDailyTripsStatus(String dailyTripsStatus) {
        DailyTripsStatus = dailyTripsStatus;
    }

    public String getDailyTripsDateTime() {
        return DailyTripsDateTime;
    }

    public void setDailyTripsDateTime(String dailyTripsDateTime) {
        DailyTripsDateTime = dailyTripsDateTime;
    }

}
