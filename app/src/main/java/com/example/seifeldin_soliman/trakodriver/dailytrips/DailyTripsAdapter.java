package com.example.seifeldin_soliman.trakodriver.dailytrips;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.seifeldin_soliman.trakodriver.R;
import com.example.seifeldin_soliman.trakodriver.driverlocation.LocationActivity;

import java.util.List;

/**
 * Created by seifeldin_soliman on 11/29/2018.
 */

public class DailyTripsAdapter extends RecyclerView.Adapter<DailyTripsAdapter.MyViewHolder> {

    public List<Trips> dailyTripsList;
    private Context contx;


    public DailyTripsAdapter(Context context, List<Trips> dailyTrips) {
        this.dailyTripsList = dailyTrips;
        this.contx = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_daily_trips_item, parent, false);
        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Trips dailyTrips = dailyTripsList.get(position);
        holder.description.setText(dailyTrips.getDailyTripsDescription());
        holder.tripStatus.setText(dailyTrips.getDailyTripsStatus());


    }

    @Override
    public int getItemCount() {
        return dailyTripsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView description,tripStatus;

        public MyViewHolder(View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.daily_trip_description_tv);

            description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Trips dailyTrips = dailyTripsList.get(getAdapterPosition());
                    if(dailyTrips.getDailyTripsStatus().equals("END")){
                        Toast.makeText(contx, "الرحلة انتهت", Toast.LENGTH_SHORT).show();
                    }else {
                        String tripId = dailyTrips.getDailyTripsId();
                        String tripStatus = dailyTrips.getDailyTripsStatus();
                        Intent intent = new Intent(contx, LocationActivity.class);
                        intent.putExtra("tripId", tripId);
                        intent.putExtra("tripStatus", tripStatus);
                        contx.startActivity(intent);
                    }
                }
            });
            tripStatus = itemView.findViewById(R.id.daily_trip_status_tv);
        }
    }
}
