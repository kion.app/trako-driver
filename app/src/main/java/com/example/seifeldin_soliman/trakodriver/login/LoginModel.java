package com.example.seifeldin_soliman.trakodriver.login;

import com.example.seifeldin_soliman.trakodriver.Status;
import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 12/25/2018.
 */

public class LoginModel {
    @SerializedName("Status")
    private Status status;
    @SerializedName("User")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
