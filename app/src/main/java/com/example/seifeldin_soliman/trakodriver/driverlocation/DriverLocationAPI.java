package com.example.seifeldin_soliman.trakodriver.driverlocation;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by seifeldin_soliman on 12/22/2018.
 */

public interface DriverLocationAPI {
    @FormUrlEncoded
    @POST("update/location")
    Call<DriverLocationModel> sendDriverLocation(@Field("tripId") String tripId,
                                                 @Field("longitude") String longitude,
                                                 @Field("latitude") String latitude,
                                                 @Field("status") String tripStatus);
}
