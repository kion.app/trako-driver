package com.example.seifeldin_soliman.trakodriver.driverlocation;

import android.app.Activity;
import android.content.DialogInterface;
import android.location.Location;

/**
 * Created by seifeldin_soliman on 12/20/2018.
 */

public class LocationContract {

    interface View{
        void updateLocation(Location location);

        void snackBarWithAction(int mainTextStringId, int actionStringId,
                                android.view.View.OnClickListener listener);
        void snackBarMessages(String message);
        void showDialog(String title,String message,String buttonText, DialogInterface.OnClickListener listener);
        Activity getApplicationInstance();
    }

    interface Presenter{

        void sendUpdatedDriverLocation(Location location, String tripStatus, String tripId);

    }

}
