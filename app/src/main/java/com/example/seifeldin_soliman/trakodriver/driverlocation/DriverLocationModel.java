package com.example.seifeldin_soliman.trakodriver.driverlocation;

import com.example.seifeldin_soliman.trakodriver.Status;
import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 12/22/2018.
 */

class DriverLocationModel {

    @SerializedName("Status")
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
