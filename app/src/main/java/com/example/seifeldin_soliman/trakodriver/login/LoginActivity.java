package com.example.seifeldin_soliman.trakodriver.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.example.seifeldin_soliman.trakodriver.R;
import com.example.seifeldin_soliman.trakodriver.RetrofitInstance;
import com.example.seifeldin_soliman.trakodriver.dailytrips.DailyTripsActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText mobileNumET, password;
    Button signUp;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mobileNumET = findViewById(R.id.email);
        password = findViewById(R.id.password);
        signUp = (Button) findViewById(R.id.btn_login);

        sharedPref = getSharedPreferences("USER_PREF", Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mobileNumber = mobileNumET.getText().toString();
                String pass = password.getText().toString();
                if (TextUtils.isEmpty(mobileNumber)) {
                    mobileNumET.setError("من فضلك ادخل رقم المحمول");
                } else if (TextUtils.isEmpty(pass)) {
                    password.setError("من فضلك ادخل كلمة السر");
                } else {
                    login(mobileNumber, pass);
                }

            }
        });
    }

    public void login(String mobileNum, String password) {

        Retrofit retrofit = RetrofitInstance.retrofitConnection();
        LoginApi branchesAPI = retrofit.create(LoginApi.class);

        Call<LoginModel> getParentInfoModelCall = branchesAPI.loginCall(mobileNum, password);

        getParentInfoModelCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {

                if (response.isSuccessful() && response.body() != null) {
                    String code = response.body().getStatus().getCode();
                    if (code.equals("200")) {
                        String userId = response.body().getUser().getUserId();
                        editor.putString("userId", userId);
                        editor.commit();
                        Intent dailyTripIntent = new Intent(LoginActivity.this, DailyTripsActivity.class);
                        startActivity(dailyTripIntent);

                    } else if (code.equals("7003")) {
                        Snackbar.make(findViewById(R.id.Login_activity), "رقم المحمول او كلمة السر خاطئة", Snackbar.LENGTH_SHORT).show();
                    } else if (code.equals("7003")) {
                        Snackbar.make(findViewById(R.id.Login_activity), "رقم المحمول او كلمة السر خاطئة", Snackbar.LENGTH_SHORT).show();
                    } else if (code.equals("7007")) {
                        Snackbar.make(findViewById(R.id.Login_activity), "لا يوجد رحلات فى الوقت الحالى", Snackbar.LENGTH_SHORT).show();
                    } else if (code.equals("7008")) {
                        Snackbar.make(findViewById(R.id.Login_activity), "هذة الشركة تم ايقاف تفعيلها", Snackbar.LENGTH_SHORT).show();
                    }else  {
                        Snackbar.make(findViewById(R.id.Login_activity), "خطأ عام", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.Login_activity), "فشل فى استرداد البيانات", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Snackbar.make(findViewById(R.id.Login_activity), "عفوا, فشل فى تحميل البيانات , برجاء التاكد من جودة الانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });
    }


}

