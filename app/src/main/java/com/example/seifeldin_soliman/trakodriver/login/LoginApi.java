package com.example.seifeldin_soliman.trakodriver.login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by seifeldin_soliman on 12/25/2018.
 */

public interface LoginApi {

    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> loginCall(@Field("mobile") String mobile, @Field("password") String password);
}
