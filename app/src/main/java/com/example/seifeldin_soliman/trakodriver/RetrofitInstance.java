package com.example.seifeldin_soliman.trakodriver;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by seifeldin_soliman on 12/22/2018.
 */

public class RetrofitInstance {

    private static final int CONNECT_TIMEOUT = 5;
    private static final int WRITE_TIMEOUT = 5;
    private static final int READ_TIMEOUT = 5;

    public static Retrofit retrofitConnection() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MINUTES)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.MINUTES)
                .readTimeout(READ_TIMEOUT, TimeUnit.MINUTES)
                .build();

        return new Retrofit.Builder()
                .baseUrl("https://tracko.app/api/driver/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}

