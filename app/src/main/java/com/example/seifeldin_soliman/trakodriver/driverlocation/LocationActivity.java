package com.example.seifeldin_soliman.trakodriver.driverlocation;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.seifeldin_soliman.trakodriver.R;
import com.example.seifeldin_soliman.trakodriver.dailytrips.DailyTripsActivity;

import java.io.Serializable;

public class LocationActivity extends AppCompatActivity implements LocationContract.View {

    Button startBt, endBT;
    LocationPresenter locationPresenter;
    String tripId;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        locationPresenter = new LocationPresenter(this);
        startBt = findViewById(R.id.trip_start_bt);
        endBT = findViewById(R.id.trip_end_bt);
        final Intent intent = getIntent();
        tripId = intent.getStringExtra("tripId");
        String tripStatus = intent.getStringExtra("tripStatus");

        if(tripStatus.equals("START")){
            locationPresenter.start("START", tripId);
            startBt.setEnabled(false);
            startBt.setBackgroundColor(getResources().getColor(R.color.colorGray1));
        }
        //Button Click
        startBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPresenter.start("START", tripId);
                startBt.setEnabled(false);
                startBt.setBackgroundColor(getResources().getColor(R.color.colorGray1));


            }
        });

        endBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPresenter.stopLocationUpdates("END",tripId);
                endBT.setEnabled(false);

                Intent tripHistoryIntent = new Intent(LocationActivity.this, DailyTripsActivity.class);
                startActivity(tripHistoryIntent);
                LocationActivity.this.finish();
            }
        });

    }

    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void snackBarWithAction(int mainTextStringId, int actionStringId, View.OnClickListener listener) {

            Snackbar.make(
                    findViewById(android.R.id.content),
                    getString(mainTextStringId),
                    Snackbar.LENGTH_LONG)
                    .setAction(getString(actionStringId), listener).show();
    }

    @Override
    public void snackBarMessages(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.location_layout), message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void showDialog(String title, String message, String buttonText, DialogInterface.OnClickListener listener) {


    }

    @Override
    public Activity getApplicationInstance() {
        return this;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (locationPresenter != null) {
            locationPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (locationPresenter != null) {
            locationPresenter.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent tripHistoryIntent = new Intent(LocationActivity.this, DailyTripsActivity.class);
                startActivity(tripHistoryIntent);
                this.finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
